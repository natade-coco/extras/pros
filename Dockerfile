FROM amd64/ubuntu:focal

RUN apt update && apt install -y ca-certificates curl

WORKDIR /root/

COPY bin /root
ADD docker-entrypoint.sh /root

CMD [ "./docker-entrypoint.sh" ]